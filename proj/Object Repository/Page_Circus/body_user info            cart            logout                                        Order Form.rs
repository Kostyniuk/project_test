<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_user info            cart            logout                                        Order Form</name>
   <tag></tag>
   <elementGuidId>e128bf82-27af-4410-b26f-7f1da99cab49</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>

    
        
            
            
            
        
    


    
    
        
            user info
            cart
            logout
        
    
    
    
        
            Order Form
            
                
                    
                    
                        
                            
                                Date From
                                
                            
                        

                        
                            
                                Date To
                                
                            
                        

                        
                            
                                Time:
                                 - 
                                720 : 12007201200
                            
                        

                        
                            
                                Price:
                                 - 
                                0 : 100001000
                            
                        

                        
                            
                                Keyword
                                
                            
                        

                        
                            
                        
                    
                
                
            
        


    

    

    
        $('.bookBtn').click(function (event) {
             var result = {};
             var count = 0;
            $('.ticketTable input').each(function (i, input) {
                result[input.name] = input.value
                if (input.value.length > 0)
                    count += parseInt(input.value);
            });

            if (count &lt;= 0)
                return;

            $.ajax({
                url: '/booking/book/',
                type: 'post',
                dataType: 'json',
                data: JSON.stringify(result),
                contentType: 'application/json; charset=utf-8',
                success: positiveHandler,
                error: negativeHandler
            });
        });

        function positiveHandler(data) {
            if (data.status == 'success')
                alert(data.message);
        }

        function negativeHandler(data) {
            var resp = data.responseJSON;
            if (data.status &lt; 500) {
                alert(resp.message)
                return;
            }
            location.href = '/500'
        }
    




    
        All rights reserved. Circus v1.0.3 ©
    


/html[1]/body[1]/div[@class=&quot;header&quot;]</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body</value>
   </webElementXpaths>
</WebElementEntity>
